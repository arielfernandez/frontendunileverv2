// @ is an alias to /src
//import Tarjeta from './_base-card-total'
//import Footer from '@/components/Footer'
//import Tarjeta from "@/components/layouts/wcm-tarjetas/_base-card-total";

//import Btn from "@/components/Library/boton";
import { EventBus } from './event-bus.js';

export default {
  name: "ResumenMatriz",
  components: {
    //Tarjeta,
  },
  data: () => ({
    count: 0

  }),
  props: {
    idusuario: {
      type: Number,
      default: 0
    },
    nombreusuario: {
      type: String,
      default: "Na"
    },
    avatar: {
      type: String,
      default: "Na"
    },
    okdata: {
      type: Number,
      default: 0
    },
    ejecuciondata: {
      type: Number,
      default: 0
    },
    retrasadodata: {
      type: Number,
      default: 0
    },
  },
  methods: {
    clickMe() {
      if (this.okdata != 0 || this.ejecuciondata != 0 || this.retrasadodata != 0) {
        var data = {
          id: this.idusuario,
          nombre: this.nombreusuario,
          avatar: this.avatar,
          ver: true,
          grafico: [this.okdata, this.ejecuciondata, this.retrasadodata]
        };

        this.count++;
        EventBus.$emit('emittedResponsableDetalle', data);
        console.log("enviando id del usuario", this.idusuario)

      }
      else {
        this.msgNotificacion("Información", "Ningun registro de tarjeta, para el Responsable", "info");

      }



    },
    msgNotificacion(titulo, mensaje, tipo) {
      this.$notify({
        title: titulo,
        message: mensaje,
        type: tipo
      });
    }

  }
};