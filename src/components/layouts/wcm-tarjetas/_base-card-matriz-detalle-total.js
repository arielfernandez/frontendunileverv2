import axios from "axios";
import { EventBus } from './event-bus.js';

import { all } from "q";
export default {
    data() {
        return {
            loading: false,
            value: 0,
            dialog: false,
            titulo: "algo",
            search: "",
            rowsPerPageItems: [2, 8, 12, 50],
            pagination: {
                rowsPerPage: 2
            },
            textsnackbar: "My timeout is set to 2000.",
            timeout: 2000,
            data_grafico: [100, 89, 10],

            verListaTarjetas: true,
            aqui: false,

            seriesAzul: [],
            chartOptionsAzul: {
                chart: {
                    height: 120,
                    type: "bar"
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            position: "top" // top, center, bottom
                        }
                    }
                },
                dataLabels: {
                    enabled: true,
                    formatter: function (val) {
                        return val + " ";
                    },
                    offsetY: -10,
                    style: {
                        fontSize: "11px",
                        colors: ["#304758"]
                    }
                },

                xaxis: {
                    categories: ["ok", "ejecucion", "retrasada"],
                    position: "bottom",
                    axisBorder: {
                        show: false
                    },
                    axisTicks: {
                        show: false
                    },
                    crosshairs: {
                        fill: {
                            type: "gradient",
                            gradient: {
                                colorFrom: "#D8E3F0",
                                colorTo: "#BED1E6",
                                stops: [0, 100],
                                opacityFrom: 0.4,
                                opacityTo: 0.5
                            }
                        }
                    },
                    tooltip: {
                        enabled: true
                    }
                },
                yaxis: {
                    axisBorder: {
                        show: false
                    },
                    axisTicks: {
                        show: false
                    },
                    labels: {
                        show: false,
                        formatter: function (val) {
                            return val + "%";
                        }
                    }
                },
                title: {
                    text: "Monthly Inflation in Argentina, 2002",
                    floating: true,
                    offsetY: 330,
                    align: "center",
                    style: {
                        color: "#444"
                    }
                }
            },
            wcmAzul: [],
            Usuarios: [],
            UsuariosAreas: [],
            UsuarioSingle: [],
            Avatar: "",
            NombreUsuario: "",
            // MATRIZ DATA
            dataprincipal: [],
            dataok: [],
            DataModificadaOK: [],
            datajecucion: [],
            DataModificadaEJECUCION: [],
            dataretrasada: [],
            DataModificadaRETRASADA: [],
            DataModificadaTABLAMain: [],
            DataModificadaTABLAMainV2: [],
            idusuario: "",
            idusuarioKey: 0,
            anio: 2020
        };
    },
    props: {
        //idusuario: {
        // type: Number,
        // default: 0
        //}
    },
    watch: {
        loader() {
            const l = this.loader;
            this[l] = !this[l];

            setTimeout(() => (this[l] = false), 3000);

            this.loader = null;
        }
    },
    mounted() {
        EventBus.$on('emittedResponsableDetalle', data => {
            console.log("recibiendo dato detalle id usuario ", data.id)
            console.log("recibiendo dato para  ", data.ver)
            this.seriesAzul = [];
            this.data_grafico = [];
            this.wcmAzul = [];

            this.value = data;
            this.dialog = data.ver;
            this.idusuario = data.id;
            this.Avatar = data.avatar;
            this.NombreUsuario = data.nombre;
            console.log("datos de grafico", data.grafico)
            //this.data_grafico=data.grafico;

            this.wcmAzul.push({
                name: "Progreso",
                data: data.grafico
            });
            this.seriesAzul = this.wcmAzul;



        });
        //EventBus.$off('emittedResponsableDetalle');

    },
    created() {

        // console.log("DATOS FINALES",THIS.)
    },
    computed: {
        DatosObserver: function () {
            this.idusuario;
            if (this.idusuario != this.idusuarioKey) {
                this.loading = true;
                this.DataModificadaTABLAMainV2 = [];
                this.dataprincipal=[];
                this.UsuariosAreas=[];
                this.idusuarioKey = this.idusuario;
                this.listarUsuariosAreas();
                this.GetdataOkPrincipal();
            }

        },

        CurrentAvatar: function () {
            return this.Avatar;
        },
        FormateoDataTabla: function () {
            let me = this;
            if (me.dataprincipal.length > 0) {
                console.log("entro nivel 1");
                //  this.FormateoMesesDataUnionOER();
                this.FormateoMesesDataUnionOERMAIN();
                this.loading = false;
            }
        }


    },
    methods: {
        CerrarEmision() {
            //EventBus.$off('emittedResponsableDetalle');
            // this.$root.$off('emittedResponsableDetalle')
            console.log("cerrando emision Bus")
            this.dialog = false;
        },
        listarUsuariosAreas() {
            let me = this;
            axios
                .get("api/RegistrosanomaliasMatriz/MatrizDatosAreas/" + this.idusuario)
                .then(function (response) {
                    me.UsuariosAreas = response.data;
                    // console.log("Usuarios", me.UsuariosAreas);
                })
                .catch(function (error) {
                    console.log(error);
                    //catch the error, check it has a response object with lodash
                    if ((error, "response")) {
                        console.log("entro a los errroes");
                        //console.log(error);
                        if (error.message === "Network Error") {
                            //me.errorMsg("El servidor se encuentra suspendido...")
                            me.msgNotificacion("Información", "Servidor suspendido", "error");
                            /*server is down*/
                        }

                        if (error.response.status == 500) {
                            me.msgNotificacion(
                                "Información",
                                "Ocurrio un error de conexión a Internet o la red...",
                                "error"
                            );
                            // me.errorMsg(" Lo más probable es un tiempo de espera del servidor o un error de conexión a Internet..")
                        }
                        //console.log(error.response.data);
                    } else {
                        console.log(
                            "Lo más probable es un tiempo de espera del servidor o un error de conexión a Internet o la red"
                        );
                        console.log("error response property is undefined");
                    }
                });
        },
        GetAvatar() {
            let me = this;
            axios
                .get("api/RegistrosanomaliasMatriz/GetUsuarioResponsable/" + this.idusuario)
                .then(function (response) {
                    me.Avatar = response.data[0].avatar;
                    me.NombreUsuario = response.data[0].nombre;
                    //console.log("FOTO OPERADOR", response.data[0].avatar);
                })
                .catch(function (error) {
                    console.log(error);
                    //catch the error, check it has a response object with lodash
                    if ((error, "response")) {
                        console.log("entro a los errroes");
                        //console.log(error);
                        if (error.message === "Network Error") {
                            //me.errorMsg("El servidor se encuentra suspendido...")
                            me.msgNotificacion("Información", "Servidor suspendido", "error");
                            /*server is down*/
                        }
                        if (error.response.status == 500) {
                            me.msgNotificacion(
                                "Información",
                                "Ocurrio un error de conexión a Internet o la red...",
                                "error"
                            );
                            // me.errorMsg(" Lo más probable es un tiempo de espera del servidor o un error de conexión a Internet..")
                        }
                        //console.log(error.response.data);
                    } else {
                        console.log(
                            "Lo más probable es un tiempo de espera del servidor o un error de conexión a Internet o la red"
                        );
                        console.log("error response property is undefined");
                    }
                });
        },
        GetdataOkPrincipal() {
            let me = this;
            axios
                .get("/api/RegistrosanomaliasMatriz/UsuariosInformeTotal/" + this.idusuario + "/" + this.anio)
                .then(function (response) {
                    me.dataprincipal = response.data;
                    console.log("DATOS OK TARJetas principal", response.data);
                    
                })
                .catch(function (error) {
                    console.log(error);
                    //catch the error, check it has a response object with lodash
                    if ((error, "response")) {
                        console.log("entro a los errroes");
                        //console.log(error);
                        if (error.message === "Network Error") {
                            //me.errorMsg("El servidor se encuentra suspendido...")
                            me.msgNotificacion("Información", "Servidor suspendido", "error");
                            /*server is down*/
                        }

                        if (error.response.status == 500) {
                            me.msgNotificacion(
                                "Información",
                                "Ocurrio un error de conexión a Internet o la red...",
                                "error"
                            );
                            // me.errorMsg(" Lo más probable es un tiempo de espera del servidor o un error de conexión a Internet..")
                        }
                        //console.log(error.response.data);
                    } else {
                        console.log(
                            "Lo más probable es un tiempo de espera del servidor o un error de conexión a Internet o la red"
                        );
                        console.log("error response property is undefined");
                    }
                });
        },
        FormateoMesesDataUnionOERMAIN() {
            let me = this;
            var i = 0;
            me.UsuariosAreas.forEach(function (element) {
                //console.log(array[i].id)
                var item = me.dataprincipal.find(x => x.idarea === element.idarea);


                // var dataItems =
                //  itemA.totalReport + itemB.totalReport + itemC.totalReport;
                if (item) {
                    me.DataModificadaTABLAMainV2.push({
                        idarea: element.idarea,
                        nombre: element.area,
                        anio: element.anio,
                        totalok: item.ok,
                        totalejecucion: item.ejecucion,
                        totalretrasada: item.retrasado
                    });
                } else {
                    // console.log("MES sin dato", element.mes, "Total:", 0);
                    me.DataModificadaTABLAMainV2.push({
                        idarea: element.idarea,
                        nombre: element.area,
                        anio: element.anio,
                        totalok: 0,
                        totalejecucion: 0,
                        totalretrasada: 0
                    });
                }

                i++;
                //console.log(i);
            });
        },
        wcmAzulData() {
            this.wcmAzul.push({
                name: "estado",
                data: this.data_grafico
            });
            this.seriesAzul = this.wcmAzul;
        },
        tableHeaderColor({ row, column, rowIndex, columnIndex }) {
            //var cant = row.length()
            if (rowIndex === 0 && columnIndex === 0) {
                return "background-color: #1306c5;color: white;font-weight: 500;";
                azul;
            }
            if (rowIndex === 0 && columnIndex === 1) {
                return "background-color: #A3E4D7;color: green;font-weight: 500;";
                verde;
            }
            if (rowIndex === 0 && columnIndex === 2) {
                return "background-color: #F9E79F;color: #EB984E;font-weight: 500;";
                amarillo;
            }
            if (rowIndex === 0 && columnIndex === 3) {
                return "background-color: #FFB6C1;color: red;font-weight: 500;";
                rojo;
            }
            if (columnIndex === 0) {
                return "background-color: #F0F3F4;color: black;font-weight: 500;";
            }
        }
    },
    msgNotificacion(titulo, mensaje, tipo) {
        this.$notify({
            title: titulo,
            message: mensaje,
            type: tipo
        });
    }
};