import '@babel/polyfill'
import Vue from 'vue';
import App from './App.vue';
import router from './router/index'
import store from './store/index'
import vuetify from './plugins/vuetify';
import axios from 'axios';
import './plugins/element.js';
import lang from 'element-ui/lib/locale/lang/es';
import locale from 'element-ui/lib/locale';
import VueApexCharts from 'vue-apexcharts';
import WebCam from "vue-web-cam";
import VImageInput from 'vuetify-image-input';
import PictureInput from 'vue-picture-input';

Vue.component('apexchart', VueApexCharts)
Vue.use(WebCam)
Vue.component(VImageInput.name, VImageInput,PictureInput.name,PictureInput);
// configurar el lenguaje de element
locale.use(lang)

Vue.config.productionTip = false

//axios.defaults.baseURL='https://apiunileverservices.net/'
axios.defaults.baseURL='https://localhost:44351/'

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
