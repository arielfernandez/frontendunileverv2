import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store/index'
import Login from '../views/Login.vue'

import Home from '../views/Home.vue'
import Rol from '../components/GestionUsuarios/Rol.vue'
import Usuario from '../components/GestionUsuarios/Usuario.vue'
import Usuario_responsable from '../components/GestionUsuarios/Usuario_responsable.vue'
//////////////////////////// registro pamco////////////////////////////
import Programar_parada from '../components/Pamco/Programar_parada.vue'
import Daily_operation from '../components/Pamco/Daily_operation.vue'
import safety_daily from '../components/Pamco/Admin_Safety/safety_daily.vue'
import quality_daily from '../components/Pamco/Admin_Quality/quality_daily.vue'
import Perdida_pamco from '../components/Pamco/Admin_Perdidas/Perdida.vue'

import RegistroAnomalia from '../components/wcm/1_N/RegistroAnomalia.vue'
import Tecnico from '../components/wcm/1_N/RegistrosAnomalias/Tecnico.vue'
import TecnicoLista from '../components/wcm/1_N/RegistrosAnomalias/TecnicoLista.vue'
import TecnicoListaPropia from '../components/wcm/1_N/RegistrosAnomalias/TecnicoListaPropio.vue'
import TecnicoListaPropiaConfirmado from '../components/wcm/1_N/RegistrosAnomalias/TecnicoListaEsperaconfirmacion.vue'
import SupervisorLista from '../components/wcm/1_N/RegistrosAnomalias/SupervisorLista.vue'
import Tarjetas from '../components/wcm/1_N/RegistrosAnomalias/ListaInicialTarjetas.vue'
import TarjetaRoja from '../components/wcm/1_N/RegistrosAnomalias/ListRojas.vue'
import TarjetaRojaAtender from '../components/wcm/1_N/RegistrosAnomalias/ListRojasAtender.vue'
import TarjetaRojaRecibir from '../components/wcm/1_N/RegistrosAnomalias/ListRojasRecibir.vue'
import TarjetaRojaCerrar from '../components/wcm/1_N/RegistrosAnomalias/ListRojasCerrar.vue'

import TarjetaAzul from '../components/wcm/1_N/RegistrosAnomalias/AzulList.vue'
import TarjetaAzulAtender from '../components/wcm/1_N/RegistrosAnomalias/AzulAtender.vue'
import TarjetaAzulRecibir from '../components/wcm/1_N/RegistrosAnomalias/AzulRecibir.vue'
import TarjetaAzulCerrar from '../components/wcm/1_N/RegistrosAnomalias/AzulCerrar.vue'



import TarjetaAmarillaList from '../components/wcm/1_N/RegistrosAnomalias/AmarillaList.vue'
import TarjetaAmarillaAtender from '../components/wcm/1_N/RegistrosAnomalias/AmarillaAtender.vue'
import TarjetaAmarillaRecibir from '../components/wcm/1_N/RegistrosAnomalias/AmarillaRecibir.vue'
import TarjetaAmarillaCerrar from '../components/wcm/1_N/RegistrosAnomalias/AmarillaCerrar.vue'

import TarjetaVerdeList from '../components/wcm/1_N/RegistroShe/List.vue'
import TarjetaVerdeAtencion from '../components/wcm/1_N/RegistroShe/Atender.vue'
import TarjetaVerdeRecibir from '../components/wcm/1_N/RegistroShe/Recibir.vue'
import TarjetaVerdeCerrar from '../components/wcm/1_N/RegistroShe/Cerrar.vue'

import SeguimientoTarjetas from '../views/wcm-tarjetas/wcm-reportes/wcm-seguimiento-tarjeta.vue'




import ProgrmacionTarjeta from '../components/wcm/1_N/RegistrosAnomalias/ProgramacionTarjetas.vue'
import PanelTarjetas from '../components/wcm/Dashboard/Panel.vue'
import PanelResumen from '../views/wcm-tarjetas/wcm-reportes/wcm-dashboard.vue'
import PanelReportesWcm from '../views/wcm-tarjetas/wcm-reportes/wcm-reportes.vue'
import PanelReportesWcmMatriz from '../views/wcm-tarjetas/wcm-reportes/wcm-matriz.vue'
import PanelReportesWcmGraficos from '../views/wcm-tarjetas/wcm-reportes/wcm-graficos.vue'
import PanelReportesWcmResumen from '../views/wcm-tarjetas/wcm-reportes/wcm-resumen.vue'

//Rutas de plan de accion
import Plan from '../views/PlanAccion/plan-accion.vue'
import Accion from '../views/PlanAccion/accion.vue'
import MatrizPlanAccion from '../views/PlanAccion/matriz-plan-accion.vue'
import ReportePlanAccion from '../views/PlanAccion/reporte.vue'
import Accions from '../views/PlanAccion/accions.vue'
import PlanView from '../views/PlanAccion/plan-view.vue'



import Proyectos from '../components/wcm/Proyecto.vue'


//Rutas de creacion de cruds simples

import Area from '../components/wcm/Area.vue'
import Anomalia from '../components/wcm/Anomalia.vue'
import CondicionInseg from '../components/wcm/CondicionInsegura.vue'
import Suceso from '../components/wcm/Suceso.vue'
import Falla from '../components/wcm/Falla.vue'
import Maquina from '../components/wcm/1_N/Maquina.vue'
import Sector from '../components/wcm/1_N/Sector.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta : {
      libre: true
    }
  },
  {
    path: '/home',
    name: 'home',
    component: Home,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/roles',
    name: 'roles',
    component: Rol,
    meta :{
      administrador :true,
      administradorWCM :true
    }
  },
  {
    path: '/usuarios',
    name: 'usuarios',
    component: Usuario,
    meta :{
      administrador :true,
      administradorWCM :true
    }
  },
  {
    path: '/usuarios_responsables',
    name: 'usuarios_responsables',
    component: Usuario_responsable,
    meta :{
      administrador :true,
      administradorWCM :true
    }
  },
  {
    path: '/registroanomalia',
    name: 'registroanomalia',
    component: RegistroAnomalia,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/tecnico',
    name: 'tecnico',
    component: Tecnico,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/tecnicoLista',
    name: 'tecnicoLista',
    component: TecnicoLista,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/tecnicoListaPropia',
    name: 'tecnicoListaPropia',
    component: TecnicoListaPropia,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/tecnicoListaPropiaConfirmado',
    name: 'tecnicoListaPropiaConfirmado',
    component: TecnicoListaPropiaConfirmado,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/supervisorLista',
    name: 'supervisorLista',
    component: SupervisorLista,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  
  {
    path: '/tarjetas',
    name: 'tarjetas',
    component: Tarjetas,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/tarjetaroja',
    name: 'tarjetaroja',
    component: TarjetaRoja,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/tarjetarojaatender',
    name: 'tarjetarojaatender',
    component: TarjetaRojaAtender,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/tarjetarojarecibir',
    name: 'tarjetarojarecibir',
    component: TarjetaRojaRecibir,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/tarjetarojacerrar',
    name: 'tarjetarojacerrar',
    component: TarjetaRojaCerrar,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/tarjetazul',
    name: 'tarjetazul',
    component: TarjetaAzul,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/tarjetazulatender',
    name: 'tarjetazulatender',
    component: TarjetaAzulAtender,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/tarjetazulrecibir',
    name: 'tarjetazulrecibir',
    component: TarjetaAzulRecibir,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/tarjetazulcerrar',
    name: 'tarjetazulcerrar',
    component: TarjetaAzulCerrar,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  
  {
    path: '/tarjeamarillalist',
    name: 'tarjeamarillalist',
    component: TarjetaAmarillaList,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/tarjetamarillatender',
    name: 'tarjetamarillatender',
    component: TarjetaAmarillaAtender,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/tarjetamarillarecibir',
    name: 'tarjetamarillarecibir',
    component: TarjetaAmarillaRecibir,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/tarjetamarillacerrar',
    name: 'tarjetamarillacerrar',
    component: TarjetaAmarillaCerrar,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/tarjetaverdelist',
    name: 'tarjetaverdelist',
    component: TarjetaVerdeList,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/tarjetaverdeatender',
    name: 'tarjetaverdeatender',
    component: TarjetaVerdeAtencion,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/tarjetaverderecibir',
    name: 'tarjetaverderecibir',
    component: TarjetaVerdeRecibir,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/tarjetaverdecerrar',
    name: 'tarjetaverdecerrar',
    component: TarjetaVerdeCerrar,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  
  
  {
    path: '/busqueda',
    name: 'busqueda',
    component: ProgrmacionTarjeta,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/tablero',
    name: 'tablero',
    component: PanelTarjetas,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/resumen',
    name: 'resumen',
    component: PanelResumen,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/reporteswcm',
    name: 'reporteswcm',
    component: PanelReportesWcm,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/reporteswcmmatriz',
    name: 'reporteswcmmatriz',
    component: PanelReportesWcmMatriz,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/reportesgraficos',
    name: 'reportesgraficos',
    component: PanelReportesWcmGraficos,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/seguimiento-tarjeta',
    name: 'seguimiento-tarjeta',
    component: SeguimientoTarjetas,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/wcmresumen',
    name: 'wcmresumen',
    component: PanelReportesWcmResumen,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/proyectos',
    name: 'proyectos',
    component: Proyectos,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/area',
    name: 'area',
    component: Area,
    meta :{
      administrador :true,
      administradorWCM :true
    }
  },
  {
    path: '/anomalia',
    name: 'anomalia',
    component: Anomalia,
    meta :{
      administrador :true,
      administradorWCM :true
    }
  },
  {
    path: '/condicion',
    name: 'condicion',
    component: CondicionInseg,
    meta :{
      administrador :true,
      administradorWCM :true
    }
  },
  {
    path: '/suceso',
    name: 'suceso',
    component: Suceso,
    meta :{
      administrador :true,
      administradorWCM :true
    }
  },
  {
    path: '/falla',
    name: 'falla',
    component: Falla,
    meta :{
      administrador :true,
      administradorWCM :true
    }
  },
  {
    path: '/maquina',
    name: 'maquina',
    component: Maquina,
    meta :{
      administrador :true,
      administradorWCM :true
    }
  },
  {
    path: '/sector',
    name: 'sector',
    component: Sector,
    meta :{
      administrador :true,
      administradorWCM :true
    }
  },
  ////////////////////////////////////////////////plan de accion/////////////////////////////////////
  {
    path: '/plan',
    name: 'plan',
    component: Plan,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/acciones/:id',
    name: 'acciones',
    component: Accion,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/matriz-plan-accion',
    name: 'matriz-plan-accion',
    component: MatrizPlanAccion,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/reporte-plan-accion',
    name: 'reporte-plan-accion',
    component: ReportePlanAccion,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/accions',
    name: 'accions',
    component: Accions,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  {
    path: '/plan-view/:id',
    name: 'plan-view',
    component: PlanView,
    meta :{
      administrador :true,
      operador: true,
      lider :true, 
      tecnico :true, 
      ingenieria :true, 
      operadorLider :true, 
      supervisorProduccion :true, 
      supervisorMantenimiendo :true, 
      administradorWCM :true,
      responsableTarjetas: true
    }
  },
  ///////////////////////////////////////////////fin plan de accion//////////////////////////////////
  ////////////////////////////////////////////////registro pamco////////////////////////////////////
  {
    path: '/Programar_parada',
    name: 'Programar_parada',
    component: Programar_parada,
    meta :{
      administrador :true
    }
  },
  {
    path: '/Daily_operation',
    name: 'Daily_operation',
    component: Daily_operation,
    meta :{
      administrador :true
    }
  },
  ////////////////////////////////////////////////fin registro pamco////////////////////////////////////
  ///////////////////////////////////////////////////////   admin safety         ////////////////////////////////////////////
  {
    path: '/safety_daily',
    name: 'safety_daily',
    component: safety_daily,
    meta :{
      administrador :true
    }   
  },
  ///////////////////////////////////////////////////////    fin admin safety    //////////////////////////////////////////// 
  ///////////////////////////////////////////////////////   admin quality         ////////////////////////////////////////////
  {
    path: '/quality_daily',
    name: 'quality_daily',
    component: quality_daily,
    meta :{
      administrador :true
    }   
  },
  ///////////////////////////////////////////////////////    fin admin quality    ////////////////////////////////////////////
  ///////////////////////////////////////////////////////    admin perdida    ////////////////////////////////////////////
  
  {
    path: '/Perdida_pamco',
    name: 'Perdida_pamco',
    component: Perdida_pamco,
    meta :{
      administrador :true
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.libre)){
    next();
  } else if (store.state.usuario && store.state.usuario.rol[0] == 'ADMINISTRADOR' || 
    store.state.usuario && store.state.usuario.rol[1] == 'ADMINISTRADOR' || 
    store.state.usuario && store.state.usuario.rol[2] == 'ADMINISTRADOR' ||
    store.state.usuario && store.state.usuario.rol[3] == 'ADMINISTRADOR' ||
    store.state.usuario && store.state.usuario.rol[4] == 'ADMINISTRADOR'){
    if (to.matched.some(record => record.meta.administrador)){
      next();
    }
    }else 
      if (store.state.usuario && store.state.usuario.rol[0]== 'OPERADOR' ||
      store.state.usuario && store.state.usuario.rol[1]== 'OPERADOR' ||
      store.state.usuario && store.state.usuario.rol[2]== 'OPERADOR' ||
      store.state.usuario && store.state.usuario.rol[3]== 'OPERADOR' ||
      store.state.usuario && store.state.usuario.rol[4]== 'OPERADOR')
      {
        if (to.matched.some(record => record.meta.operador))
        {
          next();
        }
      }
      else 
        if (store.state.usuario && store.state.usuario.rol[0]== 'LIDER' ||
        store.state.usuario && store.state.usuario.rol[1]== 'LIDER' ||
        store.state.usuario && store.state.usuario.rol[2]== 'LIDER' ||
        store.state.usuario && store.state.usuario.rol[3]== 'LIDER' ||
        store.state.usuario && store.state.usuario.rol[4]== 'LIDER')
        
        {
          if (to.matched.some(record => record.meta.lider))
          {
            next();
          }
        }else 
          if (store.state.usuario && store.state.usuario.rol[0]== 'TECNICO' ||
          store.state.usuario && store.state.usuario.rol[1]== 'TECNICO' ||
          store.state.usuario && store.state.usuario.rol[2]== 'TECNICO' ||
          store.state.usuario && store.state.usuario.rol[3]== 'TECNICO' ||
          store.state.usuario && store.state.usuario.rol[4]== 'TECNICO')
          {
            if (to.matched.some(record => record.meta.tecnico))
            {
              next();
            }
          }else 
            if (store.state.usuario && store.state.usuario.rol[0]== 'INGENIERIA' || 
            store.state.usuario && store.state.usuario.rol[1]== 'INGENIERIA' ||
            store.state.usuario && store.state.usuario.rol[2]== 'INGENIERIA' ||
            store.state.usuario && store.state.usuario.rol[3]== 'INGENIERIA' ||
            store.state.usuario && store.state.usuario.rol[4]== 'INGENIERIA')
            {
              if (to.matched.some(record => record.meta.ingenieria))
              {
                next();
    }
  }else if (store.state.usuario && store.state.usuario.rol[0]== 'OPERADOR LIDER' ||
    store.state.usuario && store.state.usuario.rol[1]== 'OPERADOR LIDER' ||
    store.state.usuario && store.state.usuario.rol[2]== 'OPERADOR LIDER' ||
    store.state.usuario && store.state.usuario.rol[3]== 'OPERADOR LIDER' ||
    store.state.usuario && store.state.usuario.rol[4]== 'OPERADOR LIDER'){

    if (to.matched.some(record => record.meta.operadorLider)){
      next();
    }
  }else if (store.state.usuario && 
    store.state.usuario.rol[0]== 'SUPERVISOR PRODUCCION' ||
    store.state.usuario && store.state.usuario.rol[1]== 'SUPERVISOR PRODUCCION' ||
    store.state.usuario && store.state.usuario.rol[2]== 'SUPERVISOR PRODUCCION' ||
    store.state.usuario && store.state.usuario.rol[3]== 'SUPERVISOR PRODUCCION' ||
    store.state.usuario && store.state.usuario.rol[4]== 'SUPERVISOR PRODUCCION'){
    if (to.matched.some(record => record.meta.supervisorProduccion)){
      next();
    }
  }else if (store.state.usuario && 
    store.state.usuario.rol[0]== 'SUPERVISOR MANTENIMIENTO' ||
    store.state.usuario && store.state.usuario.rol[1]== 'SUPERVISOR MANTENIMIENTO' ||
    store.state.usuario && store.state.usuario.rol[2]== 'SUPERVISOR MANTENIMIENTO' ||
    store.state.usuario && store.state.usuario.rol[3]== 'SUPERVISOR MANTENIMIENTO' ||
    store.state.usuario && store.state.usuario.rol[4]== 'SUPERVISOR MANTENIMIENTO'){
    if (to.matched.some(record => record.meta.supervisorMantenimiendo)){
      next();
    }
  }else if (store.state.usuario && 
    store.state.usuario.rol[0]== 'ADMINISTRADOR WCM' ||
    store.state.usuario && store.state.usuario.rol[1]== 'ADMINISTRADOR WCM' ||
    store.state.usuario && store.state.usuario.rol[2]== 'ADMINISTRADOR WCM' ||
    store.state.usuario && store.state.usuario.rol[3]== 'ADMINISTRADOR WCM' ||
    store.state.usuario && store.state.usuario.rol[4]== 'ADMINISTRADOR WCM'){
    if (to.matched.some(record => record.meta.administradorWCM)){
      next();
    }  
  } else if (store.state.usuario && 
    store.state.usuario.rol[0]== 'RESPONSABLE DE TARJETA' ||
    store.state.usuario && store.state.usuario.rol[1]== 'RESPONSABLE DE TARJETA' ||
    store.state.usuario && store.state.usuario.rol[2]== 'RESPONSABLE DE TARJETA' ||
    store.state.usuario && store.state.usuario.rol[3]== 'RESPONSABLE DE TARJETA' ||
    store.state.usuario && store.state.usuario.rol[4]== 'RESPONSABLE DE TARJETA'){
    if (to.matched.some(record => record.meta.responsableTarjetas)){
      next();
    }  
  }else{
    next({
      
      name: 'Login'
    });
  }
})

export default router
